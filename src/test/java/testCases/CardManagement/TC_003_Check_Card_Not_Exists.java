package testCases.CardManagement;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import utilities.RestUtils;

public class TC_003_Check_Card_Not_Exists extends TestBase{
	
	
	String cardNumber = RestUtils.cardNumber();
	
	@BeforeClass
	void cardExists()
	{
		RestAssured.baseURI = dummyURI;
		httpRequest = RestAssured.given();
		
		response = httpRequest.request(Method.GET, "/card/"+cardNumber);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		Assert.assertFalse(Boolean.parseBoolean(response.toString()));
	}
	
	@Test
	@Parameters("statusCodeFail")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
