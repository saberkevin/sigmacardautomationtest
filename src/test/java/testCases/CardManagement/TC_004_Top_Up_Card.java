package testCases.CardManagement;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_004_Top_Up_Card extends TestBase{
	
	
	String cardNumber = "8472842195380183";
	String value = "40000";
	
	@SuppressWarnings("unchecked")
	@BeforeClass
	void topupCard()
	{
		RestAssured.baseURI = dummyURI;
		httpRequest = RestAssured.given();
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("number", cardNumber);
		requestParams.put("value", value);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.POST, "/card/top-up");
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		Assert.assertEquals("success", response.getBody().asString());
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
