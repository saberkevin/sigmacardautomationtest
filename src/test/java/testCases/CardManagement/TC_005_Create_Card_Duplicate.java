package testCases.CardManagement;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_005_Create_Card_Duplicate extends TestBase{
	
	String cardNumber = "8472842195380183";

	@BeforeClass
	void createCard()
	{
		RestAssured.baseURI = dummyURI;
		httpRequest = RestAssured.given();
		
		response = httpRequest.request(Method.POST, "/card/"+cardNumber);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		Assert.assertEquals("already exist", response.getBody().asString());
	}
	
	@Test
	@Parameters("statusCodeForbidden")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
