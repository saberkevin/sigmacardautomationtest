package testCases.CardManagement;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import utilities.RestUtils;

public class TC_001_Create_Card extends TestBase{
	
	String cardNumber = RestUtils.cardNumber();

	@BeforeClass
	void createCard()
	{
		RestAssured.baseURI = dummyURI;
		httpRequest = RestAssured.given();
		
		response = httpRequest.request(Method.POST, "/card/"+cardNumber);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("number").toString());
		logger.info(jsonPath.get("balance").toString());

		Assert.assertEquals(jsonPath.get("number"), cardNumber);
		Assert.assertEquals(jsonPath.get("balance"), 0);
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
