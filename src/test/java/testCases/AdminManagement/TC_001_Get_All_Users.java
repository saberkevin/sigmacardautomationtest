package testCases.AdminManagement;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_Users extends TestBase{
	
	@BeforeClass
	void cardExists()
	{
		RestAssured.baseURI = adminURI;
		httpRequest = RestAssured.given();
		
		response = httpRequest.request(Method.GET);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		String responseBody = response.getBody().asString();
		Assert.assertTrue(responseBody.contains("id"));
		Assert.assertTrue(responseBody.contains("name"));
		Assert.assertTrue(responseBody.contains("email"));
		Assert.assertTrue(responseBody.contains("phone"));
		Assert.assertTrue(responseBody.contains("password"));
		Assert.assertTrue(responseBody.contains("balance"));
		Assert.assertTrue(responseBody.contains("active"));
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
