package testCases.AdminManagement;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_002_Update_User extends TestBase {
	
	private String email;
	private String balance;
	private String active;
	
	public TC_002_Update_User(String email, String balance, String active)
	{
		this.email = email;
		this.balance = balance;
		this.active = active;
	}	
	
	@SuppressWarnings("unchecked")
	@Test
	void updateUser()
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
	
		RestAssured.baseURI = adminURI;
		httpRequest = RestAssured.given();
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("email", email);
		requestParams.put("balance", Integer.parseInt(balance));
		requestParams.put("active", Boolean.parseBoolean(active));
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.PUT);
	}
	
	@Test(dependsOnMethods = {"updateUser"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"updateUser"})
	public void checkResultAsExpected()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("email").toString());
		logger.info(jsonPath.get("balance").toString());
		logger.info(jsonPath.get("active").toString());

		Assert.assertEquals(jsonPath.get("email"), email);
		Assert.assertEquals(jsonPath.get("balance"), Integer.parseInt(balance));
		Assert.assertEquals(jsonPath.get("active"), Boolean.parseBoolean(active));
	}
	
	@Test(dependsOnMethods = {"updateUser"})
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"updateUser"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test(dependsOnMethods = {"updateUser"})
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
