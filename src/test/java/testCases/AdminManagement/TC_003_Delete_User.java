package testCases.AdminManagement;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_003_Delete_User extends TestBase{
	
	@BeforeClass
	void cardExists()
	{
		RestAssured.baseURI = adminURI;
		httpRequest = RestAssured.given();
		
		String email = "zwrdiaz@gmail.com";
		
		response = httpRequest.request(Method.DELETE,email);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	public void checkResultAsExpected()
	{
		String responseBody = response.getBody().asString();
		Assert.assertEquals("success", responseBody);
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
