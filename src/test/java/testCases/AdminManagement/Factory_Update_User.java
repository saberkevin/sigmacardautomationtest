package testCases.AdminManagement;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;

public class Factory_Update_User extends TestBase{
	
	@Factory(dataProvider="dp")
	public Object[] createInstances(String email, String balance, String active) {
		return new Object[] {new TC_002_Update_User(email,balance,active)};
	}
	
	@DataProvider(name="dp")
	public String[][] dataProvider() throws IOException {
		return getExcelData("../SigmaCardAutomationTest/src/test/java/testCases/AdminManagement/user.xlsx");
	}

}
