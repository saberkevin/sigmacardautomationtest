package testCases.TransactionHistory;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;
import utilities.RestUtils;

public class Factory_History_In_Progress extends TestBase {
	
	int idNotFound = Integer.parseInt(RestUtils.idTransaction());
	
	@Factory(dataProvider="dp")
	public Object[] createInstances(String idTransaction) {
		return new Object[] {new TC_001_Get_Transaction_History_In_Progress(idTransaction)};
	}
	
	@DataProvider(name="dp")
	String[][] testData()
	{
		String idData[][] = new String[2][1];
		
		idData[0][0] = String.valueOf(1);
		idData[1][0] = String.valueOf(idNotFound);
		
		return idData;
	}
}
