package testCases.TransactionHistory;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_002_Get_Transaction_History_Completed extends TestBase{
	
	private int id;
	
	public TC_002_Get_Transaction_History_Completed(String id)
	{
		this.id = Integer.parseInt(id);
	}
	
	@Test
	void getHistoryCompleted()
	{
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		httpRequest.header("Authorization", tokenBypass);
		
		response = httpRequest.request(Method.GET, "/transaction/completed/"+id);
	}
	
	@Test(dependsOnMethods = {"getHistoryCompleted"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"getHistoryCompleted"})
	public void checkResultAsExpected()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("message").toString());
		logger.info("id="+id);
		String statusCode = jsonPath.get("code").toString();
		String responseBody = response.getBody().asString();
		
		if(statusCode.equals("200") && !jsonPath.get("data").toString().equals("[]"))
		{
			Assert.assertTrue(responseBody.contains("id"));
			Assert.assertTrue(responseBody.contains("userId"));
			Assert.assertTrue(responseBody.contains("method"));
			Assert.assertTrue(responseBody.contains("cardNumber"));
			Assert.assertTrue(responseBody.contains("value"));
			Assert.assertTrue(responseBody.contains("fee"));
			Assert.assertTrue(!responseBody.contains("IN_PROGRESS"));
			Assert.assertTrue(responseBody.contains("COMPLETED"));
			Assert.assertTrue(responseBody.contains("CANCELLED"));
		}
		else if(statusCode.equals("404"))
		{
			Assert.assertEquals("not found", jsonPath.get("message").toString());
		}
	}
	
	@Test(dependsOnMethods = {"getHistoryCompleted"})
	void assertStatusCode()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("code").toString());
		String sc = jsonPath.get("code").toString();
		
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"getHistoryCompleted"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
