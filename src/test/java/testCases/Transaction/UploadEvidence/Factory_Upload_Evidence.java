package testCases.Transaction.UploadEvidence;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;
import utilities.RestUtils;

public class Factory_Upload_Evidence extends TestBase {
	
	int idNotFound = Integer.parseInt(RestUtils.idTransaction());
	
	@Factory(dataProvider="dp")
	public Object[] createInstances(String idTransaction) {
		return new Object[] {new TC_001_Upload_Evidence_Transfer(idTransaction)};
	}
	
	@DataProvider(name="dp")
	String[][] testData()
	{
		String idData[][] = new String[3][1];
		
		idData[0][0] = String.valueOf(getInProgressIDTransaction(true));
		idData[1][0] = String.valueOf(getCompletedIDTransaction(true));
		idData[2][0] = String.valueOf(idNotFound);
		
		return idData;
	}
}
