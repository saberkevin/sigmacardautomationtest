package testCases.Transaction.UploadEvidence;

import java.io.File;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_001_Upload_Evidence_Transfer extends TestBase{
	
	private int idTransaction;
	
	public TC_001_Upload_Evidence_Transfer(String idTransaction)
	{
		this.idTransaction = Integer.parseInt(idTransaction);
	}
	
	@Test
	void uploadEvidence() throws Exception
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
	
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		File image = new File("../SigmaCardAutomationTest/src/test/java/testCases/Transaction/UploadEvidence/testimage.jpg");
		
		httpRequest.header("Authorization", tokenBypass);
		httpRequest.multiPart(image);
		
		response = httpRequest.request(Method.POST,"/transaction/upload/" + idTransaction);
	}
	
	@Test(dependsOnMethods = {"uploadEvidence"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"uploadEvidence"})
	public void checkResultAsExpected()
	{
		logger.info(idTransaction);
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("message").toString());
		String statusCode = jsonPath.get("code").toString();
		
		if(statusCode.equals("201")) 
		{
			Assert.assertEquals(jsonPath.get("message"), "success");
		}
		else if(statusCode.equals("404"))
		{
			Assert.assertEquals(jsonPath.get("message"), "transaction not found");
		}
		else if(statusCode.equals("400"))
		{
			Assert.assertEquals(jsonPath.get("message"), "bad transaction method or status");
		}
	}
	
	@Test(dependsOnMethods = {"uploadEvidence"})
	void assertStatusCode()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("code").toString());
		String sc = jsonPath.get("code").toString();
		
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"uploadEvidence"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
