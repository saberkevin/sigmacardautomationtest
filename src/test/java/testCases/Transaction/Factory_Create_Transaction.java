package testCases.Transaction;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;

public class Factory_Create_Transaction extends TestBase{
	
	@Factory(dataProvider="dp")
	public Object[] createInstances(String cardNumber, String idTopUpNumber, String method) {
		return new Object[] {new TC_001_Create_Transaction(cardNumber,idTopUpNumber,method)};
	}
	
	@DataProvider(name="dp")
	public String[][] dataProvider() throws IOException {
		return getExcelData("../SigmaCardAutomationTest/src/test/java/testCases/Transaction/transaction.xlsx");
	}

}
