package testCases.Transaction.CancelTransaction;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;
import utilities.RestUtils;

public class Factory_Cancel_Transaction extends TestBase {
	
	int idNotFound = Integer.parseInt(RestUtils.idTransaction());
	int idAnother = 41;
	
	@Factory(dataProvider="dp")
	public Object[] createInstances(String idTransaction) {
		return new Object[] {new TC_001_Cancel_Transaction(idTransaction)};
	}
	
	@DataProvider(name="dp")
	String[][] testData()
	{
		String idData[][] = new String[4][1];
		
		idData[0][0] = String.valueOf(getInProgressIDTransaction(false));
		idData[1][0] = String.valueOf(getCompletedIDTransaction(false));
		idData[2][0] = String.valueOf(idNotFound);
		idData[3][0] = String.valueOf(idAnother);
		
		return idData;
	}
}
