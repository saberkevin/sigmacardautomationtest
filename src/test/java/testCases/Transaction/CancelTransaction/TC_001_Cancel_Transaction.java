package testCases.Transaction.CancelTransaction;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_001_Cancel_Transaction extends TestBase{
	
	private int idTransaction;
	
	public TC_001_Cancel_Transaction(String idTransaction)
	{
		this.idTransaction = Integer.parseInt(idTransaction);
	}
	
	@Test
	void cancelTransaction()
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
	
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		httpRequest.header("Authorization", tokenBypass);
		
		response = httpRequest.request(Method.DELETE,"/transaction/" + idTransaction);
	}
	
	@Test(dependsOnMethods = {"cancelTransaction"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"cancelTransaction"})
	public void checkResultAsExpected()
	{
		logger.info(idTransaction);
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("message").toString());
		String statusCode = jsonPath.get("code").toString();
		
		if(statusCode.equals("200")) 
		{
			Assert.assertEquals(jsonPath.get("message"), "success");
		}
		else if(statusCode.equals("404"))
		{
			Assert.assertEquals(jsonPath.get("message"), "transaction not found");
		}
		else if(statusCode.equals("400"))
		{
			Assert.assertEquals(jsonPath.get("message"), "can't cancel completed transaction");
		}
		else if(statusCode.equals("401"))
		{
			Assert.assertEquals(jsonPath.get("message"), "transaction belong to another user");
		}
	}
	
	@Test(dependsOnMethods = {"cancelTransaction"})
	void assertStatusCode()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("code").toString());
		String sc = jsonPath.get("code").toString();
		
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"cancelTransaction"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
