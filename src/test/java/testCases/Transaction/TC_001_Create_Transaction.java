package testCases.Transaction;

import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_001_Create_Transaction extends TestBase{
	
	private String cardNumber;
	private int idTopUpOption;
	private String method;
	private int balance;
	
	public TC_001_Create_Transaction(String cardNumber, String idTopUpOption, String method)
	{
		this.cardNumber = cardNumber;
		this.idTopUpOption = Integer.parseInt(idTopUpOption);
		this.method = method;
	}
	
	@BeforeClass
	void init()
	{
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		httpRequest.header("Authorization", tokenBypass);
	}
	
	@BeforeTest
	void getWalletBalance()
	{
		if(method.equals("WALLET"))
		{
			response = httpRequest.request(Method.GET, "/profile/");
			Map<String, String> data = response.jsonPath().getMap("data");
			balance = Integer.parseInt(String.valueOf(data.get("balance")));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void createTransaction()
	{
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("cardNumber", cardNumber);
		requestParams.put("idTopUpOption", idTopUpOption);
		requestParams.put("method", method);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.POST, "/transaction");
		
		
	}
	
	@Test(dependsOnMethods = {"createTransaction"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"createTransaction"})
	public void checkResultAsExpected()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("code").toString());
		logger.info(jsonPath.get("message").toString());
		String sc = jsonPath.get("code").toString();
		String message = jsonPath.get("message").toString();
		
		if(sc.equals("201")) 
		{
			if(method.equals("WALLET") && balance >= 22000)
			{
				Assert.assertTrue(Integer.parseInt(jsonPath.get("balance").toString()) < balance);
			}	
			Assert.assertEquals("success", message);
		}
		else if(sc.equals("400")) Assert.assertEquals("not enough balance", message);
	}
	
	@Test(dependsOnMethods = {"createTransaction"})
	void assertStatusCode()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("code").toString());
		String sc = jsonPath.get("code").toString();
		
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"createTransaction"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
