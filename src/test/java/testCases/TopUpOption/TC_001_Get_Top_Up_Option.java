package testCases.TopUpOption;

import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_Top_Up_Option extends TestBase{
	
	@Test
	void getTopUpOption()
	{
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		httpRequest.header("Authorization", tokenBypass);
		
		response = httpRequest.request(Method.GET, "/topup-option");
	}
	
	@Test(dependsOnMethods = {"getTopUpOption"})
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test(dependsOnMethods = {"getTopUpOption"})
	public void checkResultAsExpected()
	{
		String responseBody = response.getBody().asString();
		Assert.assertTrue(responseBody.contains("code"));
		Assert.assertTrue(responseBody.contains("message"));
		Assert.assertTrue(responseBody.contains("id"));
		Assert.assertTrue(responseBody.contains("value"));
		Assert.assertTrue(responseBody.contains("fee"));
	}
	
	@Test(dependsOnMethods = {"getTopUpOption"})
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test(dependsOnMethods = {"getTopUpOption"})
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test(dependsOnMethods = {"getTopUpOption"})
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
