package testCases.Register;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;

public class Factory_Login extends TestBase{
	@Factory(dataProvider="login")
	public Object[] createInstances(String emailOrPhone,String password) {
		return new Object[] {new TC_003_Login(emailOrPhone,password)};
	}
	
	@DataProvider(name="login")
	public String[][] dataProvider() throws IOException {
		return getExcelData("../SigmaCardAutomationTest/src/test/java/testCases/Register/login.xlsx");
	}
}
