package testCases.Register;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_001_Register extends TestBase{
	
	private String name;
	private String email; 
	private String phone; 
	private String password; 
	
	public TC_001_Register(String name, String email, String phone, String password) {
		this.name=name;
		this.email=email;
		this.phone=phone;
		this.password=password;
	}
	
	@SuppressWarnings("unchecked")
	@BeforeClass
	void registerUser()
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
		
		RestAssured.baseURI = prodURI;
		httpRequest = RestAssured.given();
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("name", name);
		requestParams.put("email", email);
		requestParams.put("phone", phone);
		requestParams.put("password", password);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.POST, "/register");
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}

	@Test
	void checkAccountElement()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("name").toString());
		logger.info(jsonPath.get("email").toString());
		logger.info(jsonPath.get("phone").toString());
		logger.info(jsonPath.get("password").toString());
		
		Assert.assertEquals(jsonPath.get("name"), name);
		Assert.assertEquals(jsonPath.get("email"), email);
		Assert.assertEquals(jsonPath.get("phone"), phone);
		Assert.assertEquals(jsonPath.get("password"), password);
	}
	
	@Test
	void checkPasswordValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String checkPassword = jsonPath.get("password");
		logger.info(checkPassword);
		
		String regex = "^(?=.[0-9])(?=.[a-z])(?=.[A-Z])(?=.[!@#$%^&*])(?=\\\\S+$).{8,}$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(checkPassword);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	

	@Test
	void checkEmailValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String checkEmail = jsonPath.get("email");
		logger.info(checkEmail);
		
		String regex = "^(.+)@(.+).(.+)$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(checkEmail);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	@Test
	void checkUsernameValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String checkName = jsonPath.get("name");
		logger.info(checkName);
		
		String regex = "^([a-zA-Z]\\s){2,50}$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(checkName);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	@Test
	void checkPhoneNumber()
	{
		JsonPath jsonPath = response.jsonPath();
		String checkPhone = jsonPath.get("phone");
		logger.info(checkPhone);
		
		String regex = "^628[0-9]{10,14}$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(checkPhone);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}