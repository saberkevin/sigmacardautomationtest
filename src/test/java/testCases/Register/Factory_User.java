package testCases.Register;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import base.TestBase;

public class Factory_User extends TestBase{
	@Factory(dataProvider="account")
	public Object[] createInstances(String name, String email, String phone, String password) {
		return new Object[] {new TC_001_Register(name,email,phone,password)};
	}
	
	@DataProvider(name="account")
	public String[][] dataProvider() throws IOException {
		return getExcelData("../SigmaCardAutomationTest/src/test/java/testCases/Register/account.xlsx");
	}
}
